# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/extra/x86_64/libssh/
## Maintainer : Antonio Rojas <arojas@archlinux.org>
## Contributor: Tom Gundersen <teg@jklm.no>
## Contributor: Andrea Scarpino <andrea@archlinux.org>
## Contributor: ice-man <icemanf@gmail.com>
## Contributor: sergeantspoon <sergeantspoon@archlinux.us>
#--------------------------------------------------------------------------------------

pkgbase=libssh
pkgname=(libssh libssh-docs)
pkgver=0.8.7
pkgrel=2
arch=('x86_64')
license=('LGPL')
_website="https://www.libssh.org/"
pkgdesc="Library for accessing ssh client services through C libraries"
url="https://www.libssh.org/files"
source=("$url/${pkgver%.*}/$pkgname-$pkgver.tar.xz"{,.asc})

depends=(
    'zlib'
    'libressl')

makedepends=(
    'cmake'
    'cmocka'
    'doxygen'
    'python')

#--------------------------------------------------------------------------------------
prepare() {

    ## disable the test. It is confused by our clean container setup.
    ## 'extra-x86-build' uses user 'nobody' that has a record in /etc/passwd file
    ## but $HOME envvar is set to '/build'. 
    ## The test expects that $HOME corresponds to passwd file.
    sed 's/cmocka_unit_test(torture_path_expand_tilde_unix),//' \
     -i libssh-${pkgver}/tests/unittests/torture_misc.c

    mkdir -p build
}

build() {
    cd build

    cmake ../$pkgname-$pkgver       \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DWITH_GSSAPI=OFF           \
        -DUNIT_TESTING=ON
    make
    make docs
}

check() {
    cd build

    make test
}

package_libssh() {
    cd build

    make DESTDIR="$pkgdir" install
}

package_libssh-docs() {
    pkgdesc="Documentation for libssh"
    depends=()

    mkdir -p "$pkgdir"/usr/share/doc/libssh
    cp -r build/doc/html "$pkgdir"/usr/share/doc/libssh
}

#--------------------------------------------------------------------------------------
validpgpkeys=('8DFF53E18F2ABC8D8F3C92237EE0FC4DCC014E3D' # Andreas Schneider <asn@cryptomilk.org>
)
sha512sums=('e91d1f4c1343aa232ade0fe4b5e9a92ca65e3716f4ebe2ec25b04def4fae5a3774349f05a6919836551f66fb0288ed6a3e19e0ab786c081616218be973356522'
            'SKIP')
